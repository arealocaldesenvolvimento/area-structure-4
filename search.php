<?php get_header() ?>
    <section class="al-container padrao" role="main">
		<?php get_template_part('searchform') ?>
        <?php if (have_posts()) : ?>
            <header class="entry-title">
                <h1>Todas as postagens sobre: <span><?= the_search_query() ?></span></h1>
            </header>
	</section>
            <?php get_template_part('loop', 'search') ?>
        <?php else: ?>
            <header class="entry-title">
                <h1>Nada encontrado</h1>
            </header>

            <div class="entry-content">
                <p class="notfound-message">Desculpe, mas nada foi encontrado para sua busca</p>
            </div>
	</section>
        <?php endif ?>

<?php get_footer() ?>
