<?php get_header() ?>

    <section class="al-container padrao" role="main">
        <?php while (have_posts()): the_post() ?>
            <article id="id-<?php the_ID() ?>" <?php post_class() ?>>
                <header class="entry-title">
                    <h1 class="title"><?php the_title() ?></h1>
                </header>

                <div class="entry-content">
                    <?php the_content() ?>
                </div>
            </article>
        <?php endwhile ?>
    </section>

<?php get_footer() ?>
