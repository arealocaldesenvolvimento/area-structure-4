<?php

/**
 * Imports.
 */
include_once get_theme_file_path('app/controllers/CityController.php');
include_once get_theme_file_path('app/controllers/StateController.php');

/**
 * Declaração de rotas da API.
 */
function routes()
{
    $namespace = 'api';
    
    register_rest_route($namespace, '/estados', [
        'methods' => 'GET',
        'callback' => 'getStates',
    ]);

    register_rest_route($namespace, '/cidades', [
        'methods' => 'POST',
        'callback' => 'getCities',
    ]);
}

// WP REST API start
add_action('rest_api_init', 'routes');
