<?php

flush_rewrite_rules();


function examplePostType()
{
    register_post_type('post-type-name', [
        'labels' => [
            'name' => _x('Name', 'post type general name'),
            'singular_name' => _x('Name', 'post type singular name'),
            'add_new' => __('Adicionar novo'),
            'add_new_item' => __('Adicionar novo'),
            'edit_item' => __('Editar'),
            'new_item' => __('Novo'),
            'view_item' => __('Ver'),
            'not_found' => __('Nada encontrado'),
            'not_found_in_trash' => __('Nada encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Estatutos e Regimentos',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions'],
    ]);
}
// add_action('init', 'examplePostType');
