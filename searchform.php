<form id="search-form" role="search" method="get" action="<?= home_url('/') ?>">
    <label for="s">
        <input type="search" list="list-search" value="<?= isset($_GET['s']) ? $_GET['s'] : ""?>" name="s" id="s" placeholder="Faça uma busca">
		<?php
			$posts_2 = new WP_Query(array(
				'post_type' => 'post',
				'posts_per_page' => -1
			));
		?>
		<?php if(!empty($posts_2->posts)): ?>
			<datalist id="list-search">
				<?php
					$titulos=[];
					foreach($posts_2->posts as $post):
						$titulos[]= $post->post_title;
					endforeach;
					foreach(array_unique($titulos) as $titulo): ?>
					<option value="<?= $titulo ?>">
				<?php
					endforeach;
				?>
			</datalist>
		<?php endif;?>
    </label>
    <button id="search-submit">Buscar</button>
</form>
