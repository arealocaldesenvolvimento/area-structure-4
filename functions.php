<?php

/* Helpers
========================================================================== */
// include_once get_theme_file_path('app/helpers/...')

/* App
========================================================================== */
include_once get_theme_file_path('app/config.php');
include_once get_theme_file_path('app/scripts/post-types.php');

/* Actions & Filters
========================================================================== */
add_action('show_admin_bar', '__return_false');
add_filter('widget_text', 'do_shortcode');
add_filter('retrieve_password_message', "change_template_password_recovery", 10, 4 );
add_filter( 'show_admin_bar', fn() => ! current_user_can( 'manage_options' ) && ! is_admin() ? false : true );
add_action('wp_enqueue_scripts', fn() => wp_enqueue_script('jquery'));
remove_action('wp_head', 'wp_generator');

/* Theme functions
========================================================================== */

// Define a página 'Home' como a página inicial
add_action('after_switch_theme', function() {
    if ('1' !== get_option('my_theme_activated')) {
		update_option('page_on_front', get_page_by_title('Home')->ID ?? wp_insert_post([
			'post_title'    => 'Home',
			'post_content'  => '',
			'post_status'   => 'publish',
			'post_type'     => 'page'
		]));
		update_option('show_on_front', 'page');
    }
});

function wp_mail_return_texthtml()
{
    return 'text/html';
}
add_filter('wp_mail_content_type', 'wp_mail_return_texthtml');


function get_image_url($file = '')
{
    return get_template_directory_uri().'/assets/images/'.$file;
}


function image_url($file = '')
{
    echo get_image_url($file);
}

add_filter('upload_mimes', function($mimes){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});

/**
 * Change Template recovery password.
 */
function change_template_password_recovery( $message, $key, $user_login, $user_data ){
    $message = file_get_contents(get_template_directory().'/app/tamplate-recovery-password.html');
    $message = str_replace("[logo]", '<a href="'.get_site_url().'" title="'.get_bloginfo('name').' - '.get_bloginfo('description').'" ><img src="'.get_image_url('logo.webp').'" alt="'.get_bloginfo('name').'"></a>', $message);
    $message = str_replace("[nome]", $user_login, $message);
    $message = str_replace("[site-url]", get_site_url(), $message);
    $message = str_replace("[password-reset-link]", network_site_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_login), 'login'), $message);

    return $message;
}

/**
 * Print the image of the logo, if it is the home place a wrap of h1.
 */
function the_logo($img = 'logo.webp'){
	$blog_name = get_bloginfo('name');
	$image_path = get_template_directory().'/assets/images/'.$img;
	$image_type = pathinfo($image_path, PATHINFO_EXTENSION);
	$image_size = getimagesize($image_path);

	if ($image_type === 'svg'): ?>
		<a href="<?= home_url('/') ?>" title="<?= $blog_name.' - '.get_bloginfo('description') ?>" class="logo">
			<?= file_get_contents($image_path) ?>
		</a>
	<?php else: ?>
		<a href="<?= home_url('/') ?>" title="<?= $blog_name.' - '.get_bloginfo('description') ?>" class="logo">
			<img src="<?= get_image_url($img) ?>" alt="<?= $blog_name ?>" width="<?= $image_size[0] ?>" height="<?= $image_size[1] ?>">
		</a>
	<?php endif;
}

/**
 * Change text at the bottom of the panel.
 */
function change_panel_footer_text()
{
    echo '&copy; <a href="http://www.arealocal.com.br/" target="_blank">&Aacute;rea Local</a> - Sites - E-commerce';
}
add_filter('admin_footer_text', 'change_panel_footer_text');

/**
 * Change the login form logo.
 */
function change_login_form_logo()
{
    echo '<style>.login h1 a{background-image:url('.get_image_url('logo-login-form.png').')!important;}</style>';
}
add_action('login_enqueue_scripts', 'change_login_form_logo');

/**
 * Change the login form logo url.
 */
function login_form_logo_url()
{
    return get_home_url();
}
add_filter('login_headerurl', 'login_form_logo_url');

/**
 * Standardizes login error message, so as not to show when the user exists.
 */
function wrong_login()
{
    return '<b>ERRO</b>: Usuário ou senha incorretos.';
}
add_filter('login_errors', 'wrong_login');

/**
 * Change title of the login form logo.
 */
function login_form_logo_url_title()
{
    return get_bloginfo('name').' - Desenvolvido por Área Local';
}
add_filter('login_headertext', 'login_form_logo_url_title');

/**
 * Adds main navigation, html5 support and post thumbnail.
 */
function al_setup()
{
    register_nav_menus(['principal' => 'Navegação Principal']);
    add_theme_support('post-thumbnails');
    add_theme_support('html5', ['comment-list', 'comment-form', 'search-form', 'gallery', 'caption']);
}
add_action('after_setup_theme', 'al_setup');

/**
 * Add home in the menu.
 */
function show_home_menu($args)
{
    $args['show_home'] = true;

    return $args;
}
add_filter('wp_page_menu_args', 'show_home_menu');

/**
 * Records widget areas.
 */
function add_widget_areas()
{
    register_sidebar([
        'name' => 'Área de Widget Primária',
        'id' => 'area-widget-primaria',
        'description' => 'Área de Widget Primária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ]);

    register_sidebar([
        'name' => 'Área de Widget Secundária',
        'id' => 'area-widget-secundaria',
        'description' => 'Área de Widget Secundária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ]);
}
add_action('widgets_init', 'add_widget_areas');

/**
 * Disables wordpress emojis.
 */
function disable_wp_emojicons()
{
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
}
add_action('init', 'disable_wp_emojicons');

function sanitize_filename($filename)
{
    $ext = explode('.', $filename);
    $ext = end($ext);
    $sanitized = preg_replace('/[^a-zA-Z0-9-_.]/', '', substr($filename, 0, -(strlen($ext) + 1)));
    $sanitized = str_replace('.', '-', $sanitized);

    if (function_exists('sanitize_title')) {
        $sanitized = sanitize_title($sanitized);
    }

    return strtolower($sanitized.'.'.$ext);
}
add_filter('sanitize_file_name', 'sanitize_filename', 10);


function custom_short_excerpt($excerpt, $length = 200)
{
    $text = substr($excerpt, 0, $length);

    if (strlen($excerpt) > $length) {
        $text .= ' ...';
    }

    return $text;
}
add_filter('the_excerpt', 'custom_short_excerpt');

/**
 * Change excerpt tag.
 */
function change_excerpt_more()
{
    return '<a title="'.get_the_title().'" href="'.get_permalink().'" class="more-link">Ler mais</a>';
}
add_filter('excerpt_more', 'change_excerpt_more');

/**
 * Add admin.js and admin.css script to the admin screen.
 */
function adminScriptsStyles()
{
    wp_enqueue_style('admin-styles', get_bloginfo('template_url').'/assets/css/admin.css');
    wp_enqueue_script('admin-functions', get_bloginfo('template_url').'/assets/js/admin.js');
}
add_action('admin_enqueue_scripts', 'adminScriptsStyles');

/**
 * Render Área Local help panel.
 */
function custom_dashboard_help()
{
    echo '
		<p>
			Bem-vindo ao tema Área Local! precisa de ajuda?</br> Contate o suporte
			<a target="_blank" href="https://suporte-arealocal.tomticket.com/">aqui!</a>
		</p>
		<h2>Contato</h2>
		<p>Telefone/Whastapp: <a target="_blank" href="https://wa.me/554735219850"><b>(47) 3521-9850</b></a></p>
		<p>E-mail:
			<a target="_blank" href="mailto:contato@arealocal.com.br"><b>contato@arealocal.com.br</b></a>
		</p>
	';
}

/**
 * Enables service area on the Wordpress dashboard.
 */
function my_custom_dashboard_widgets()
{
    wp_add_dashboard_widget('atendimento-arealocal', 'Atendimento Área Local', 'custom_dashboard_help');
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');


function get_thumbnail_url($post_id, $size)
{
    if (!isset($post_id)) {
        $post_id = get_the_ID();
    }
    if (has_post_thumbnail($post_id)) {
        $post_thumbnail_url = get_the_post_thumbnail_url('', $size);
    } else {
        $post_thumbnail_url = get_image_url('default.webp');
    }

    return $post_thumbnail_url;
}

/**
 * Render pagination links.
 */
function paginationLinks($query, $url = '')
{
    $big = 999999999;
    $url = empty($url) ? str_replace($big, '%#%', esc_url(get_pagenum_link($big))) : $url.'page/%#%/';

    if ($query->max_num_pages > 1) {
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links([
            'base' => $url,
            'show_all' => false,
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $query->max_num_pages,
            'prev_text' => '<i class="fa-solid fa-chevron-left"></i>',
            'next_text' => '<i class="fa-solid fa-chevron-right"></i>',
        ]);
    }
}

/**
 * Adequate pagination for single-post and archive-post.
 */
function custom_posts_per_page($query)
{
    global $pagenow;

    if ($query->is_archive('post') && 'edit.php' !== $pagenow) {
        set_query_var('posts_per_page', 1);
    }
}
add_action('pre_get_posts', 'custom_posts_per_page');

/**
 * Change some archive page title.
 */
function customArchiveTitles($title)
{
    if (is_post_type_archive('some_archive')) {
        return 'Some Archive';
    }

    return $title;
}
add_filter('wp_title', 'customArchiveTitles');
add_filter('get_the_archive_title', 'customArchiveTitles');

/**
 * Custom actions before template rendering.
 */
function preTemplateAction($template)
{
    return $template;
}
add_filter('template_include', 'preTemplateAction');

// Change admin panel color scheme
wp_admin_css_color(
    'area-structure-4',
    __('Área Structure 4'),
    get_bloginfo('template_url').'/assets/css/al-admin-color-scheme.min.css',
    [
        '#222',
        '#e0e047',
        '#00244c',
        '#19539a',
    ],
    [
        'base' => '#e5f8ff',
        'focus' => '#fff',
        'current' => '#fff',
    ]
);

/**
 * Set "area-structure-4" as default color scheme.
 */
function setDefaultAdminColor($userId)
{
    wp_update_user([
        'ID' => $userId,
        'admin_color' => 'area-structure-4',
    ]);
}
add_action('user_register', 'setDefaultAdminColor');

/**
 * Redirecionamento costumizado, conforme categoria ou tax
 */
function template_redirect($template) {
    if(is_tax('tax-name')) {
        $template = get_query_template('page-name');
    }

    if(is_home()) {
        $template = get_query_template('page-name');
    }

    if(is_archive('post-type')) {
        $template = get_query_template('page-name');
    }

   return $template;
}
//add_filter('template_include', 'template_redirect');

//desabilita comentários
function remove_comments()
{
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'remove_comments');

// Gera iframe do youtube
function generateYouTubeEmbed($url)
{
	if (preg_match('/^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9_-]{11})/', $url, $match)) {
		$videoId = $match[1];

		$iframe = '<iframe width="100%" height="315" src="https://www.youtube.com/embed/' . $videoId . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';

		return $iframe;
	} else {
		return 'URL inválida do YouTube.';
	}
}

// Remove formatação de Telefone
function removePhoneFormat($numero){
	$numero = preg_replace('/[^0-9]/', '', $numero);
	return $numero;
}

// Página de opções
if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title'    => __('Opções do Tema'),
		'menu_title'    => __('Opções do Tema'),
		'menu_slug'     => 'opcoes-do-tema',
		'parent_slug'   => 'options-general.php',
		'capability'    => 'edit_posts',
		'redirect'      => false
	));
}

// Obtem id do youtube
function getYtId($url){
	$pattern = '/(?:\/|v=|vi=|youtu.be\/|\/embed\/|\/v\/|\/e\/|youtu.be\/|v=|vi=|e\/|youtu.be\/|embed\/|v=|vi=|e\/|youtu.be\/|embed\/)([^#\&\?]*).*/';
	preg_match($pattern, $url, $matches);
	return $matches[1];
}

// Obtem a url da thumbnail do youtube
function getYtUrlThumb($id){
	return "https://img.youtube.com/vi/$id/hqdefault.jpg";
}


/**
 * Verifica se uma variável está verdadeiramente vazia.
 *
 * Se a variável não for um array ou objeto, a função retornará o resultado da função built-in `empty()`.
 * Se a variável for um array ou objeto, a função verifica se todos os seus elementos, propriedades e sub-elementos estão vazios.
 *
 * @param mixed $var A variável a ser verificada.
 * @return bool Retorna `true` se a variável estiver vazia ou se todos os seus elementos, propriedades e sub-elementos estiverem vazios, caso contrário retorna `false`.
 */
function trulyEmpty($var) {
    if (is_array($var) || is_object($var)) {
        foreach ($var as $value) {
            if (!trulyEmpty($value)) {
                return false;
            }
        }
        return true;
    }
    return empty($var);
}

/**
 * Imprime uma imagem responsiva usando o elemento <picture>.
 *
 * @param array $args {
 *     Argumentos opcionais.
 *
 *     @type int    $image_id       O ID da imagem principal. (obrigatório)
 *     @type int    $mobile_image_id O ID da imagem para dispositivos móveis.
 *     @type string $default_size   O tamanho da imagem principal (e.g. 'medium', 'large').
 *     @type string $mobile_size    O tamanho da imagem para dispositivos móveis (e.g. 'thumbnail').
 *     @type string $alt            O texto alternativo para a imagem.
 * }
 * @return void
 */
function print_responsive_image($args = []) {
    // Valores padrão
    $defaults = [
        'image_id'       => null,
        'mobile_image_id'=> null,
        'default_size'   => 'full',
        'mobile_size'    => 'medium',
        'alt'            => 'Imagem'
    ];

    $args = wp_parse_args($args, $defaults);

    static $js_printed = false;

    if (!$args['image_id']) return;

    $image_src = wp_get_attachment_image_src($args['image_id'], $args['default_size']);
    $image_full_src = wp_get_attachment_image_src($args['image_id'], 'full');
    $image_meta = wp_get_attachment_metadata($args['image_id']);
    $sizes = $image_meta['sizes'];

    echo '<picture>';

    if ($args['mobile_image_id']) {
        $mobile_image_src = wp_get_attachment_image_src($args['mobile_image_id'], $args['mobile_size']);
        echo '<source media="(max-width: 480px)" srcset="' . esc_url($mobile_image_src[0]) . '">';
    }

    echo '<source srcset="' . esc_url($image_full_src[0]) . '">';

    foreach ($sizes as $image_size_name => $image_size_data) {
        if ($image_size_data['width'] > 480) {
            $image_size_src = wp_get_attachment_image_src($args['image_id'], $image_size_name);
            echo '<source srcset="' . esc_url($image_size_src[0]) . '">';
        }
    }

    echo '<img src="' . esc_url($image_src[0]) . '" width="' . esc_attr($image_src[1]) . '" height="' . esc_attr($image_src[2]) . '" alt="' . esc_attr($args['alt']) . '" onload="adjustImageDimensions(this)">';

    echo '</picture>';

    if (!$js_printed) {
        echo "
        <script>
            function adjustImageDimensions(imgElement) {
                const currentSrc = imgElement.currentSrc;

                fetch(currentSrc).then(response => {
                    return response.blob();
                }).then(blob => {
                    let img = new Image();
                    img.src = URL.createObjectURL(blob);
                    img.onload = function() {
                        imgElement.width = img.width;
                        imgElement.height = img.height;
                    };
                });
            }
        </script>
        ";
        $js_printed = true;
    }
}
#Gerador de slider, o parametro é o próprio field do slide, encontra-se na raiz do projeto para importação
function generateSwiperSlider($slides = [], $pagination = true, $navigationButtons = [true, true], $scrollbar = false){
    echo '<div class="swiper">';
    echo '<div class="swiper-wrapper">';
    foreach($slides as $key => $slide){
        if($slide['ativo']){
            echo '<div class="swiper-slide">';
                if(!$slide['informacoes']['label_botao'] && $slide['informacoes']['url']){
                    echo '<a href="'.$slide['informacoes']['url'].'" target="'.(($slide['informacoes']['nova_guia'])? "_blank":"").'">';
                }
                print_responsive_image([
                    'image_id'       => $slide['imagens']['desktop']['id'],
                    'mobile_image_id'=> $slide['imagens']['mobile']['id'],
                    'default_size'   => 'full',
                    'mobile_size'    => 'full',
                    'alt'            => 'Imagem'
                ]);
                echo '<div class="al-container">';
                echo $slide['informacoes']['texto']? '<div class="informacoes">'.$slide['informacoes']['texto'].'</div>':'';
                if($slide['informacoes']['label_botao']){
                    echo '<div class="botao">
                            <a href="'.$slide['informacoes']['url'].'" target="'.(($slide['informacoes']['nova_guia'])? "_blank":"").'">'.
                                $slide['informacoes']['label_botao']
                            .'</a>
                        </div>';
                }
                if(!$slide['informacoes']['label_botao'] && $slide['informacoes']['url']){
                    echo '</a>';
                }
            echo '</div></div>';
        }
    }
    echo '</div>';
    echo $pagination ? '<div class="swiper-pagination"></div>':"";
    echo $scrollbar ? '<div class="swiper-scrollbar"></div>':"";
    echo $navigationButtons[0] ? '<div class="swiper-button-prev"></div>':"";
    echo $navigationButtons[1] ? '<div class="swiper-button-next"></div>':"";
    echo '</div>';
}
/* WP REST
========================================================================== */
include_once get_theme_file_path('app/routes.php');
