# AreaStructure 4.0 - Tema personalizado

![alt](screenshot.jpg)

- Usar CDNs preferivelmente tanto para JS quanto para CSS

- Antes de iniciar um novo projeto remova o diretório oculto ".git" em estrutura-basica/

- Compilar assets e/ou minificá-los (NPM):
```language
npm i
npm run watch
npm run production
```
