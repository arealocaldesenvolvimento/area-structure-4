<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!-- CDNs CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" integrity="sha512-yJHCxhu8pTR7P2UgXFrHvLMniOAL5ET1f5Cj+/dzl+JIlGTh5Cz+IeklcXzMavKvXP8vXqKMQyZjscjf3ZDfGA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>
	<!-- END CDNs CSS -->
    <link rel="stylesheet" type="text/css" media="all" href="<?= get_template_directory_uri(); ?>/style.css">
	<?php if (!function_exists('wpseo_init')): ?>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title>
			<?= is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
		</title>
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<link rel="canonical" href="<?= get_permalink(); ?>">
		<meta property="og:locale" content="<?php bloginfo('language'); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php wp_title(''); ?> - <?php bloginfo('name'); ?>">
		<meta property="og:description" content="<?php bloginfo('description'); ?>">
		<meta property="og:url" content="<?= get_permalink(); ?>">
		<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
	<?php endif; ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" aria-level="1">
		<section class="al-container">
			<div id="desktop-menu">
				<?= the_logo("logo.svg") ?>
				<nav class="menu desktop">
					<?php wp_nav_menu(array('menu'=> 'principal', 'container' => false)); ?>
				</nav>
			</div>
            <div id="mobile-menu">
				<div class="container">
					<div class="close">
						<a id="close">
							<svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M21.2132 22.6274L6.19888e-06 1.41422L1.41422 3.21865e-06L22.6274 21.2132L21.2132 22.6274Z" fill="white"/>
								<path d="M0 21.2132L21.2132 0L22.6274 1.41421L1.41421 22.6274L0 21.2132Z" fill="white"/>
							</svg>
						</a>
					</div>
					<?= the_logo("logo.svg") ?>
					<nav class="menu mobile">
						<?php wp_nav_menu(array('menu'=> 'principal', 'container' => false)); ?>
					</nav>
                </div>
			</div>
			<a href="#mobile-icon" id="mobile-icon" class="mobile">
				<svg width="30" height="16" viewBox="0 0 30 16" fill="none" xmlns="http://www.w3.org/2000/svg">
					<rect width="30" height="2" fill="#26160F"/>
					<rect y="7" width="30" height="2" fill="#26160F"/>
					<rect y="14" width="30" height="2" fill="#26160F"/>
				</svg>
			</a>
        </section>
	</header>
    <div class="overlay"></div>
    <!-- Wrapper -->
    <div id="wrapper">
