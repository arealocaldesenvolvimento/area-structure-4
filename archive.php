<?php get_header() ?>
	<?php
	$filtro = '';
	if(!empty($_GET['filtro'])){
		$filtro = array (
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $_GET['filtro'],
		);
	}else if (!empty(get_queried_object()->term_id)) {
		$filtro = array (
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => get_queried_object()->term_id,
		);
	}
	$paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
	$posts = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 5,
		'orderby' => 'date',
		'tax_query' => array($filtro),
		'meta_query' => array(
			array(
				'key'   => 'destaque',
				'value' => true,
			)
			),
		'order' => 'DESC'
	)); ?>
	<?php if ($posts->have_posts()): ?>
		<section class="al-container blog destaques">
			<h1 class="title">Destaques do Blog</h1>
			<div class="left-right-content">
			<?php $index = 1;
			while($posts->have_posts()) : $posts->the_post(); ?>
				<?php if ($index == 1): ?>
					<div class="left">
						<div class="article-container">
							<article class="artigo" id="id-<?php the_ID(); ?>">
								<a href="<?= get_the_permalink() ?>" title="<?=  get_the_title() ?>">
									<img src="<?=  get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?=  get_the_title() ?>"/>
									<div class="titulo">
										<h1 class="title"><?=  get_the_title() ?></h1>
									</div>
								</a>
							</article>
						</div>
					</div>
					<div class="right">
				<?php else: ?>
						<div class="left-right-content">
							<div class="left">
								<a href="<?= get_the_permalink() ?>" title="<?=  get_the_title() ?>">
									<img src="<?=  get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?=  get_the_title() ?>"/>
								</a>
							</div>
							<div class="right">
								<h2><?=  get_the_title() ?></h1>
							</div>
						</div>
				<?php endif;?>
			<?php $index++;
			endwhile;
			wp_reset_postdata(); ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php $posts = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 12,
		'orderby' => 'date',
		'tax_query' => array($filtro),
		'order' => 'DESC',
		'paged'  => $paged
	)); ?>
	<section class="al-container blog listagem">
		<h1 class="title">Veja todos os posts</h1>
		<div class="posts">
			<?php if ($posts->have_posts()):
				while($posts->have_posts()) : $posts->the_post(); ?>
					<div class="article-container">
						<article class="artigo" id="id-<?php the_ID(); ?>">
							<div class="img-container">
								<img src="<?=  get_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title() ?>"/>
							</div>
							<div class="titulo">
								<a href="<?= get_permalink() ?>" title="<?= get_the_title() ?>"><h2><?= get_the_title() ?></h2></a>
							</div>
							<div class="text">
								<?php the_excerpt(); ?>
							</div>
						</article>
					</div>
				<?php endwhile; else: ?>
				<p>Nenhum post encontrado com os filtros atuais</p>
			<?php endif; ?>
		</div>
		<nav class="load_more">
			<?= paginationLinks($posts) ?>
		</nav>
	</section>
<?php get_footer() ?>
