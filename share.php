<div class="compartilhe">
	<span>Compartilhe</span>
	<div class="links">
		<a id="copy" link="<?= get_permalink() ?>">
			<img src="<?= get_image_url("anchor.png") ?>" alt="Ícone de link">
		</a>
		<?= do_shortcode('[addtoany]') ?>
	</div>
</div>
