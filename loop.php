
<?php if(have_posts()): ?>
	<section class="al-container blog loop listagem">
		<div class="posts">
			<?php while (have_posts()) : the_post() ?>
				<div class="article-container">
					<article class="artigo" id="id-<?php the_ID(); ?>">
						<div class="img-container">
							<img src="<?=  get_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title() ?>"/>
						</div>
						<div class="titulo">
							<a href="<?= get_permalink() ?>" title="<?= get_the_title() ?>"><h2><?= get_the_title() ?></h2></a>
						</div>
						<div class="entry-date">
							<time><?php the_time('d/m/Y') ?></time>
						</div>
						<div class="text">
							<?php the_excerpt(); ?>
						</div>
						<?php if (count(get_the_category())) : ?>
							<div class="entry-category">
								<span class="category-links">
									<?= get_the_category()[0]->name ?>
								</span>
							</div>
						<?php endif ?>
					</article>
				</div>
			<?php endwhile  ?>
		</div>
		<nav class="load_more">
			<?= paginationLinks($wp_query) ?>
		</nav>
	</section>
<?php endif; ?>
