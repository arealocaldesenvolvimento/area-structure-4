    </div>

    <!-- Footer -->
    <footer></footer>

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?= addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?= addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`

		/* CDNs JS */
		var scripts = [
            'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.24/sweetalert2.all.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js',
            'https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js',
            '<?= get_template_directory_uri(); ?>/public/js/app.js?att=<?= rand(); ?>'
        ]

        document.addEventListener('DOMContentLoaded', function() {
           loadScripts(scripts);
        }, false);

        function loadScripts(scripts) {
            let time = 0;
            scripts.forEach(element => {
                time += 50;
                setTimeout(function () {
                    let script  = document.createElement('script');
                    script.type  = 'text/javascript';
                    script.src  = element;
                    document.getElementsByTagName('body')[0].appendChild(script);
                }, time);
            });
        }
    </script>
    <!-- CDNs JS -->
    <!-- Font Awesome 6 -->
    <!-- <script src="https://kit.fontawesome.com/a82ebcfe38.js" crossorigin="anonymous"></script> -->
    <!-- Jquery 3.6.0 -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- Jquery Mask (Máscara para campos gerais) -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.8/jquery.mask.min.js" integrity="sha512-hAJgR+pK6+s492clbGlnrRnt2J1CJK6kZ82FZy08tm6XG2Xl/ex9oVZLE6Krz+W+Iv4Gsr8U2mGMdh0ckRH61Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- Jquery MaskMoney (Máscara para monetários) -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/jquery-maskmoney@3.0.2/dist/jquery.maskMoney.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- Jquery Validate (validação de formulários) -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- Cleave JS (validação e mascara de cartão de crédito) -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.0.2/cleave.min.js" integrity="sha512-SvgzybymTn9KvnNGu0HxXiGoNeOi0TTK7viiG0EGn2Qbeu/NFi3JdWrJs2JHiGA1Lph+dxiDv5F9gDlcgBzjfA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- Sweetalert2 (pop-ups) -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.4.24/sweetalert2.all.min.js" integrity="sha512-Ty04j+bj8CRJsrPevkfVd05iBcD7Bx1mcLaDG4lBzDSd6aq2xmIHlCYQ31Ejr+JYBPQDjuiwS/NYDKYg5N7XKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- Lightslider (Slider)-->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js" integrity="sha512-Gfrxsz93rxFuB7KSYlln3wFqBaXUc1jtt3dGCp+2jTb563qYvnUBM/GP2ZUtRC27STN/zUamFtVFAIsRFoT6/w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
    <!-- END CDNs JS -->
    <!-- <script type="text/javascript" src="<?= get_template_directory_uri(); ?>/public/js/vendor.js"></script> -->
    <?php wp_footer(); ?>
</body>
</html>
