/**
 * Global theme functions
 */

jQuery(document).ready(function(){
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
	if(document.querySelector("#copy")){
		document.querySelector("#copy").addEventListener("click", ()=>{
			var TextToCopy = document.querySelector("#copy").attributes.link.value;
			var TempText = document.createElement("input");
			TempText.value = TextToCopy;

			document.body.appendChild(TempText);
			TempText.select();

			document.execCommand("copy");
			document.body.removeChild(TempText);
			Swal.fire({
				icon: 'sucess',
				title: 'Texto copiado com sucesso',
			})
		});
	}
	if(document.querySelector(".swiper")){
		const swiper = new Swiper('.swiper', {
			direction: 'horizontal',
			loop: true,
			pagination: {
			el: '.swiper-pagination',
			},
			navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
			},
			scrollbar: {
			el: '.swiper-scrollbar',
			},
		});
	}
});

// abertura de menu
jQuery('#mobile-icon').click(function (event) {
	event.preventDefault();
	jQuery('.overlay').toggleClass('active');
	jQuery('#mobile-menu').toggleClass('active');
});

// faz a rolagem suave de links ancora internos
jQuery('.menu a[href*=#]').click(function (e) {
	var target = jQuery("#"+e.target.href.slice(e.target.href.indexOf("#") + 1));
	if (target.length) {
		jQuery('html, body').animate({ scrollTop: target.offset().top }, 500);
	}
    jQuery('.overlay').removeClass('active');
    jQuery('#mobile-menu').removeClass('active');
});

// fechar menu clicando fora
window.addEventListener('click', function(e){
	if (jQuery('#mobile-menu').hasClass('active')) {
		if (!document.getElementById('mobile-menu').contains(e.target) && !document.querySelector('#mobile-icon').contains(e.target)){
			jQuery('.overlay').toggleClass('active');
			jQuery('#mobile-menu').toggleClass('active');
		}
	}
});

// fechamento do menu
jQuery('#close').click(function () {
	jQuery('.overlay').toggleClass('active');
	jQuery('#mobile-menu').toggleClass('active');
});

