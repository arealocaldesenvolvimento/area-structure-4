<?php get_header() ?>
	<section class="al-container single">
		<h1 class="title"><?= get_the_title() ?></h1>
		<span class="data"><?= get_the_date() ?></span>
		<div class="img-container">
			<img src="<?=  get_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?= get_the_title() ?>"/>
		</div>
		<div class="the-content">
			<?php the_content(); ?>
		</div>
		<?php #get_template_part('share') ?>
		<?php #get_template_part('fancy-comments') ?>
		<?php #get_template_part('comments') ?>
	</section>
<?php get_footer() ?>
